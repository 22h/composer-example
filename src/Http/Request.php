<?php

namespace App\Http;

class Request
{

    public function __construct(private string $userAgent)
    {
    }

    public function getUserAgent(): string
    {
        return $this->userAgent;
    }
}
