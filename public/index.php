<?php

use App\Http\Request;
use DeviceDetector\DeviceDetector;

include('../vendor/autoload.php');

$request = new Request(
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36'
);

$dd = new DeviceDetector($request->getUserAgent());
$dd->parse();

echo $dd->getClient('name').PHP_EOL;
